FROM ubuntu:22.04
LABEL name="maxiaoxiao" email="tttxiaoz@163.com"

ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Asia/Shanghai

COPY .tmp/sources.list /etc/apt/sources.list.bak

WORKDIR /home/workspace

# install base
RUN set -ex; \
    mv /etc/apt/sources.list /etc/apt/sources.list.ubuntu; \
    mv /etc/apt/sources.list.bak /etc/apt/sources.list; \
    apt-get update; \
    apt-get upgrade -y; \
    apt-get install -y build-essential cmake gdb; \
    apt-get install -y git wget vim libssl-dev; \
    apt-get install -y netcat net-tools python3; \
    apt-get install -y llvm clang clangd; \
    apt-get install -y zlib1g-dev; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*

COPY ./resources /tmp/resources

# go env
RUN set -x; \
    cd /tmp/resources; \
    bash /tmp/resources/prepared_go_env.sh

# compile source
RUN set -x; \
    cd /tmp/resources; \
    for lib in \
    googletest \
    gflags \
    glog \
    protobuf \
    ; do bash /tmp/resources/build_libs.sh $lib; done; \
    ldconfig; \
    ln -s /usr/local/bin/cmake /usr/bin/cmake; \
    rm -rf /tmp/*
