#!/bin/bash
set -ex

usage() {
    echo "This script builds third-party libraries."
    echo ""
    echo "Usage: $0 lib_name"
    echo "Example: $0 opencv_4_5"
    exit 1
}

if [[ $# -eq 0 ]] || [[ "$1" == "-h" || "$1" == "--help" ]]; then
    usage  
fi

target=$1
source_dir="/tmp/resources"
build_nproc=$(nproc)  # Use number of available cores

build_cmake() {
    local dir=cmake-3.24.0
    mkdir $dir
    tar zxf ${source_dir}/${dir}.tar.gz -C ./${dir} --strip-components=1
    pushd $dir
    ./bootstrap
    make install -j "$build_nproc"
    popd
}

build_protobuf() {
    local dir=protobuf-21.5
    mkdir $dir
    tar zxf ${source_dir}/${dir}.tar.gz -C ./${dir} --strip-components=1
    pushd $dir
    cmake -S . -B build
    cmake --build build -j "$build_nproc"
    cmake --install build
    popd
}

build_gflags() {
    local dir=gflags-2.2.2
    mkdir $dir
    tar zxf ${source_dir}/${dir}.tar.gz -C ./${dir} --strip-components=1
    pushd $dir
    cmake -S . -B build -DBUILD_SHARED_LIBS=ON -DINSTALL_HEADERS=ON -DINSTALL_SHARED_LIBS=ON
    cmake --build build -j "$build_nproc"
    cmake --install build
    popd
}

build_glog() {
    local dir=glog-1.6.0
    mkdir $dir
    tar zxf ${source_dir}/${dir}.tar.gz -C ./${dir} --strip-components=1
    pushd $dir
    cmake -S . -B build
    cmake --build build -j "$build_nproc"
    cmake --install build
    popd
}

build_libevent() {
    local dir=libevent-2.1.12
    mkdir $dir
    tar zxf ${source_dir}/${dir}.tar.gz -C ./${dir} --strip-components=1
    pushd $dir
    cmake -S . -B build
    cmake --build build -j "$build_nproc"
    cmake --install build
    popd
}

build_googletest() {
    local dir=googletest-1.12.1
    mkdir $dir
    tar zxf ${source_dir}/${dir}.tar.gz -C ./${dir} --strip-components=1
    pushd $dir
    cmake -S . -B build
    cmake --build build -j "$build_nproc"
    cmake --install build
    popd
}

build_"${target}"
