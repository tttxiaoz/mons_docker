#!/bin/bash
set -ex

registry_url="registry.cn-beijing.aliyuncs.com/monolith_docker"

current_path=$(dirname "$0")
base_path=$(cd "${current_path}/.." && pwd)

version=$(head -1 "${base_path}/VERSION")
architecture=$(uname -m)

docker_name="mons_dev_${architecture}:${version}"
docker_path="${base_path}"

tmp_path="${base_path}/.tmp"
sources_list="sources.list.${architecture}"

mkdir -p "$tmp_path"

cp "${base_path}/tools/$sources_list" "$tmp_path/sources.list"

docker build -t "$docker_name" "$docker_path"

rm -rf "$tmp_path"

full_docker_name="${registry_url}/${docker_name}"

docker tag "$docker_name" "$full_docker_name"
docker push "$full_docker_name"