#!/bin/bash
set -e
current_path=$(dirname "$0")

base_path=$(cd "${current_path}/.." && pwd)

mkdir -p "${base_path}/resources"

source_list="${current_path}/source_list.json"

if [ -f "$source_list" ]; then
    jq -c '.[]' "$source_list" | while read -r line; do
        name=$(jq -r '.name' <<< "$line")
        version=$(jq -r '.version' <<< "$line")
        uri=$(jq -r '.uri' <<< "$line")
        tar_name="${base_path}/resources/${name}-${version}.tar.gz"
        if [[ ! -f "$tar_name" ]]; then
            echo "Downloading $name -> $tar_name"
            wget -O "$tar_name" "$uri" -q --show-progress
        fi
    done
fi

echo "prepare source done."